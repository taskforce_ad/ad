docker run --rm --gpus all -it\
    --mount type=bind,source=/home/dawudmaxx/ad,target=/workspace\
    --mount type=bind,source=/mnt/ACTIVE,target=/data\
    --mount type=bind,source=/home/dawudmaxx/.bashrc_custom,target=/home/dawudmaxx/.bashrc_custom\
    --env "PIPENV_VENV_IN_PROJECT"="enabled"\
    --name "ad"\
    ubuntu:prod