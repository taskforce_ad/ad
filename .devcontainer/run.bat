docker run --rm --gpus all -it^
    --mount type=bind,source=D:/DEV/04_Entwirkler,target=/workspace^
    --mount type=bind,source=Y:/DATA,target=/data^
    --env "PIPENV_VENV_IN_PROJECT"="enabled"^
    --name "ad"^
    ubuntu:prod_11.4.1