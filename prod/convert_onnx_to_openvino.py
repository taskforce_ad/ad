from os.path import *
import os
import subprocess

onnx_model_path = '/home/dawudmaxx/ad/prod/model-semseg-1686265938-epoch100/model.onnx'
openvino_modeldir = join(dirname(onnx_model_path), 'openvino')
if not exists(openvino_modeldir):
    os.mkdir(openvino_modeldir)

subprocess.run(["mo", "--input_model",  onnx_model_path, "--output_dir",  openvino_modeldir,"--input_shape", "[2, 512, 1024, 3]"])
