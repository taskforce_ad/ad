import onnxruntime
import numpy as np


def onnx_predict(onnx_model_path, input_data):
    session = onnxruntime.InferenceSession(onnx_model_path)

    # Prepare input data
    input_name = session.get_inputs()[0].name
    # input_data = np.random.rand(3, 512, 1024, 3).astype(np.float32)  # Replace with your input data

    # Run inference
    output_name = session.get_outputs()[0].name
    output = session.run([output_name], {input_name: input_data})

    output = np.squeeze(np.array(output))
    return output
