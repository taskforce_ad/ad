from flask import Flask, request, jsonify
from openvino.inference_engine import IECore
import numpy as np
import time

app = Flask(__name__)

# Load the IR model
model_xml = '/home/dawudmaxx/ad/prod/model-semseg-1686265938-epoch100/openvino/model.xml'
model_bin = '/home/dawudmaxx/ad/prod/model-semseg-1686265938-epoch100/openvino/model.bin'
ie = IECore()
net = ie.read_network(model=model_xml, weights=model_bin)
exec_net = ie.load_network(network=net, device_name='CPU')

@app.route('/inference', methods=['POST'])
def inference():
    input_layer_name = 'input_1'
    output_layer_name = 'conv2d_10'

    # Get input data from the request
    input_data = np.array(request.json['input_data'])
    # print(f'input_data shape: {input_data.shape}')

    start = time.perf_counter()
    # Perform inference
    output = exec_net.infer(inputs={input_layer_name: input_data})
    print(f'Inference took {time.perf_counter() - start} secs')
    # print(f'output_data shape {output[output_layer_name].shape}')

    return jsonify(output[output_layer_name].tolist())

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
