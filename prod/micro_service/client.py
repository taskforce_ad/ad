import requests
import json
import numpy as np
import time

# Define the endpoint URL
url = 'http://localhost:5000/inference'


input_list = np.random.rand(2, 512, 1024, 3).astype(np.float32).tolist()

# Convert the list to a JSON string
json_data = json.dumps({'input_data':input_list})

start_time = time.perf_counter()
# Make the POST request
response = requests.post(url, data=json_data, headers={'Content-Type': 'application/json'})
print(f'Response received in {time.perf_counter() - start_time} secs')

# Check the response
if response.status_code == 200:
    # Request was successful
    print(np.array(response.json()).shape)
else:
    # Request failed
    print('Request failed with status code:', response.status_code)
