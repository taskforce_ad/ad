from os.path import *
import subprocess

savedmodel_path = '/home/dawudmaxx/ad/prod/model-semseg-1686265938-epoch100'
onnx_path = join(dirname(savedmodel_path), 'model.onnx')

subprocess.run(["python", "-m", "tf2onnx.convert", "--saved-model", savedmodel_path, "--output", onnx_path])