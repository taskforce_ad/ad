from os.path import *
from openvino.inference_engine import IECore
import numpy as np

def openvino_predict(openvino_modeldir, input_array):
    # Load the Inference Engine
    ie = IECore()

    # Load the optimized model (IR files)
    model_xml = join(openvino_modeldir, 'model.xml')
    model_bin = join(openvino_modeldir, 'model.bin')
    net = ie.read_network(model=model_xml, weights=model_bin)

    # Load the network to the plugin
    exec_net = ie.load_network(network=net, device_name='CPU')

    # Prepare input data
    input_data = {'input_1': input_array}

    # Perform inference
    output = exec_net.infer(inputs=input_data)

    # Process the output
    output_array = output['conv2d_10']

    # print(output_array.shape)

    # Process the output array as needed

    # Clean up
    del exec_net
    del ie
    return output_array

if __name__ == '__main__':
    input_array = np.random.rand(2, 512, 1024, 3).astype(np.float32)
    openvino_predict(input_array)
