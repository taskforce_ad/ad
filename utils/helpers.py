import numpy as np
import tensorflow as tf
from collections import namedtuple
import tensorflow.keras.utils as utils
from glob import glob
import json

from os import makedirs
from os.path import realpath, dirname, join, exists

shape = namedtuple('shape', ['height', 'width'])

def load_mnist_dataset(nb_classes=10):
    (x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
    x_train, x_test = x_train / 255.0, x_test / 255.0
    y_train, y_test = utils.to_categorical(y_train, nb_classes), utils.to_categorical(y_test, nb_classes)
    x_train = x_train.reshape(x_train.shape[0], x_train.shape[1], x_train.shape[2], 1)
    x_test = x_test.reshape(x_test.shape[0], x_test.shape[1], x_test.shape[2], 1)
    return x_train.astype(np.float32), y_train, x_test.astype(np.float32), y_test, x_train.shape[0], x_test.shape[0]

def get_output_dir(filename, outputdirname):
    return join(dirname(realpath(filename)), outputdirname)

def get_root_ad():
    return join(dirname(realpath(__file__)), '..')

def get_model_from_path(model_path, compile=False):
    model = tf.keras.models.load_model(model_path, compile=compile)
    return model

class ModelManager():
    def __init__(self, filename, outputdirname, stage):
        outputdir_path = get_output_dir(filename, outputdirname)
        self.outputdir_path = outputdir_path
        makedirs(outputdir_path, exist_ok=True)
        self.checkpoint_path = join(outputdir_path, 'checkpoints')
        makedirs(self.checkpoint_path, exist_ok=True)
        outputdir_path = join(outputdir_path, stage)
        makedirs(outputdir_path, exist_ok=True)
        self.summary_writer = tf.summary.create_file_writer(outputdir_path)

    def save_meta_data(self, modelname, epoch_num, metrics):
        metadata_filename = join(self.checkpoint_path, '{}-{:04d}.txt'.format(modelname, epoch_num))
        with open(metadata_filename, 'w+') as f:
            [f.write(f"{key} {metrics.get(key)}\n") for key in metrics]
        f.close()

    def save_pre_epoch_model(self, model, modelname, epoch, step):
        tf.keras.models.save_model(model, join(self.checkpoint_path, "pre", f"epoch-{epoch}", f"itr-{step}"))
        print(f"[INFO] Pre-NN Model: {modelname} saved")

    def get_losses_and_accuracies(self, modelname):
        meta_files = glob(join(self.checkpoint_path, f"{modelname}-*.txt"))
        meta_files.sort(reverse=False)
        epochs = []
        losses = []
        categorical_accuracies = []
        for filepath in meta_files:
            epochs.append(int(filepath[filepath.rfind('-')+1:-4]));
            with open(filepath, 'r') as f:
                    lines = f.readlines()
                    for line in lines:
                        line_content = line.rstrip().split(" ")
                        if line_content[0] == "categorical_accuracy":
                            categorical_accuracies.append(float(line_content[1]))
                        elif line_content[0] == "loss":
                            losses.append(float(line_content[1]))
        return epochs, losses, categorical_accuracies
    

    def save_model(self, model, modelname, epoch_num, metrics):
        new_accuracy = metrics.get('categorical_accuracy')
        if not epoch_num == 0:
            # list all meta data and retrience the last meta data
            meta_files = glob(join(self.checkpoint_path, f"{modelname}-*.txt"))
            meta_files.sort(reverse=True)
            if not len(meta_files) == 0:
                old_metadata_filename = meta_files[0]
                with open(old_metadata_filename, 'r') as f:
                    lines = f.readlines()
                    for line in lines:
                        line_content = line.rstrip().split(" ")
                        if line_content[0] == "categorical_accuracy":
                            self.old_categorical_accuracy = float(line_content[1])
                            break

                if self.old_categorical_accuracy > new_accuracy:
                    print(f"[INFO] NN Model:{modelname} <<<NOT>>> saved")
                    return False
        tf.keras.models.save_model(model, join(self.checkpoint_path, modelname))
        self.save_meta_data(modelname, epoch_num, metrics)
        print(f"[INFO] NN Model:{modelname} saved")
        self.old_categorical_accuracy = new_accuracy
        return True

    def get_summary_writer(self):
        return self.summary_writer

    def resume(self, modelname):
        files = glob(join(self.checkpoint_path, f'{modelname}-*.txt'))
        self.can_resume = len(files)>0
        if (self.can_resume):
            self.meta_data_files = files
            num_txt = files[-1].split('-')[-1]
            self.epoch_num = int(num_txt[:num_txt.rfind('.')])
        return self.can_resume

    def get_epoch_num(self):
        return self.epoch_num

    def get_saved_model(self, modelname):
        model = get_model_from_path(join(self.checkpoint_path, modelname))
        return model
