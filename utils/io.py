import cv2
import numpy as np
from PIL import Image

def read_color_image(inputfile):
	RGB_img = cv2.cvtColor(cv2.imread(inputfile), cv2.COLOR_BGR2RGB)
	RGB_img = np.float32(RGB_img)
	return np.asarray(RGB_img)

def read_label_image(inputfile):
    label = Image.open(input)
    label = np.float32(label)
    return np.asarray(label)	
 
def write_color_image(outputfile, img):
	RGB_img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
	RGB_img = np.uint8(RGB_img)
	cv2.imwrite(outputfile, RGB_img)

def imwrite_(fname, img):
    '''
    This function save a normal image of type uint8 and uint16
    Input: 
        fname: path to save the image including ext
        img: ndrray with type eithet 8bit or 16bit
    '''
    # save rgb image with uint8 type
    if img.dtype == 'uint16':
        cv2.iwrite(fname, img)

