from matplotlib import pyplot as plt

def plot_history_dict(history_dict, model_dirpath=None):
    fig = plt.figure(figsize=(13, 10)) # width, height in inches
    keys = list(history_dict.keys())
    size = len(keys)
    if size == 0:
        print(f'[ERROR] No keys present in the histroy dict')
        return False
    else:
        rows = size
        cols = 2
        index = 0
        for key in keys:
            index += 1
            losses = [x['loss'] for x in history_dict[key]]
            mean_ious = [x['mean_iou'] for x in history_dict[key]]
            epochs = list(range(len(history_dict[key])))
            ax1 = fig.add_subplot(rows, cols, index)
            ax1.set_title(key)
            ax2 = ax1.twinx()
            ax1.set_xlabel('Epoch')
            ax1.set_ylabel('Loss', color='r')
            ax2.set_ylabel('MeanIoU', color='g')
            ax1.plot(epochs, losses, 'r-')
            ax2.plot(epochs, mean_ious, 'g-')
            index += 1
            losses = [x['val_loss'] for x in history_dict[key]]
            mean_ious = [x['val_mean_iou'] for x in history_dict[key]]
            ax1 = fig.add_subplot(rows, cols, index)
            plt.subplots_adjust(wspace=0.4)
            plt.subplots_adjust(hspace=0.4)
            ax2 = ax1.twinx()
            ax1.set_xlabel('Epoch')
            ax1.set_ylabel('Val Loss', color='r')
            ax2.set_ylabel('Val MeanIoU', color='g')
            ax1.plot(epochs, losses, 'r-')
            ax2.plot(epochs, mean_ious, 'g-')
        if model_dirpath == None:
            plt.show()
        else:
            plt.savefig(f'{model_dirpath}/history_plot.png')
            print(f'[INFO] Plot saved in file: {model_dirpath}/history_plot.png')
        plt.close()