from utils.gpu_init import gpu_init
from os.path import abspath, dirname, sep
import sys

class INIT():
    ROOT_PATH=''
    def __init__(self, memory_growth=False, ensure_gpu=True):
        # ROOT_PATH = abspath(dirname(__file__))[:abspath(dirname(__file__)).rfind(sep+'ad'+sep)]
        self.ROOT_PATH = dirname(dirname(__file__))
        try:
            sys.path.index(self.ROOT_PATH)
        except ValueError:
            sys.path.insert(0, self.ROOT_PATH)
        if not gpu_init(memory_growth, ensure_gpu):
            print(f'[ERROR] Terminating Executing Abruptly')
            exit()

    def get_ad_root(self):
        print(f'[INFO] AD root: {self.ROOT_PATH}')
        return self.ROOT_PATH