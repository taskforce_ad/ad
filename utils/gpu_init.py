import tensorflow as tf
import os

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

def init_session(visible_device=0, memory=8):
    config = ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = memory/12
    config.gpu_options.allow_growth = False
    session = InteractiveSession(config=config)

    os.environ['DEVICE_ORDER'] = 'PCI_BUS_ID'
    os.environ['CUDA_VISIBLE_DEVICES'] = str(visible_device)
    os.environ['CUDA_DEVICE'] = str(visible_device)
    gpus = tf.config.experimental.list_physical_devices('GPU')
    logical_gpus = tf.config.experimental.list_logical_devices('GPU')
    print('{} GPUs and {} Logical GPUs'.format(len(gpus), len(logical_gpus)))
    return session

def gpu_init(memory_growth, ensure_gpu):
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            for gpu in gpus:
                print(f'[INFO] set memory_growth={memory_growth} for {gpu.name}')
                tf.config.experimental.set_memory_growth(gpu, memory_growth)
        except RuntimeError as e:
            print(e)
            return False
        return True
    else:
        print(f'[WARN] GPU not Found, Fallback to CPU')
        if not ensure_gpu:
            return True
        key_command = input('Do You Want To Continue? ')
        if key_command == 'y'or key_command == 'Y':
            return True
        else:
            return False