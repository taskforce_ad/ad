import pickle
import datetime
from os.path import *
import argparse

from utils.plot import *

def decoder_history(filepath):
    times = []
    with open(filepath, 'rb') as histrory_path:
        histroy = pickle.load(histrory_path)
    keys = list(histroy.keys())
    if len(keys):
        times = [datetime.datetime.fromtimestamp(int(x)) for x in keys]
    else:
        print(f'[ERROR] Donot have any keys in the file {filepath}')
        return {}
    print(f'[INFO] Found {len(times)} Training Histories For the SavedModel')
    [print(time) for time in times]
    return histroy

def decode_and_plot_history(modelpath, show_plot=False):
    filepath = f'{modelpath}/history.pickle'
    h1 = decoder_history(filepath)
    if show_plot:
        plot_history_dict(h1)
    else:
        plot_history_dict(h1, dirname(filepath))

# in-line local call to the function decode_and_plot_history(..)
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('modelpath', help='Abs path of the model dir')
    parser.add_argument('save_plot', nargs='?', help='Save plot', default=False)
    args = parser.parse_args()
    modelpath = args.modelpath
    save_plot = args.save_plot

    decode_and_plot_history(modelpath, show_plot=not save_plot)