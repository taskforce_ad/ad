import tensorflow as tf
import numpy as np

from tensorflow.keras.models import Model
from tensorflow.keras.layers import (
    Input,
    Conv2D, MaxPooling2D, Dense,
    Layer, Activation, Dropout, Flatten
)

class Conv2DModel(Model):
    def __init__(self, inputshape):
        super(Conv2DModel, self).__init__()
        self.Input = Input(shape=inputshape)
        self.conv2d = Conv2D(64, (3,3), activation="relu")
        self.pool = MaxPooling2D((3,3))
        self.flatten = Flatten()
        self.dense1 = Dense(1000, activation="relu")
        self.dropout = Dropout(0.2)
        self.dense2 = Dense(10, activation="softmax")
        
    def call(self, x):
        net = self.Input(x)
        net = self.conv2d(net)
        net = self.pool(net)
        net = self.flatten(net)
        net = self.dense1(net)
        net = self.dropout(net)
        net = self.dense2(net)
        return net
class DummyModel(Model):
    def __init__(self):
        super(DummyModel, self).__init__()        
        self.flatten = Flatten()
        self.dense1 = Dense(2000, activation="relu")
        self.dropout1 = Dropout(0.2)
        self.dense2 = Dense(10, activation="softmax")
    
    def call(self, x):        
        net = self.flatten(x)
        net = self.dense1(net)
        net = self.dropout1(net)
        net = self.dense2(net)
        return net