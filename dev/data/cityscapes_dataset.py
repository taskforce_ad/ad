import tensorflow as tf
import numpy as np
import os.path as path
import time
from tqdm import tqdm
from PIL import Image, ImageDraw
from matplotlib import pyplot as plt
from dev.data.cityscapesScripts.cityscapesscripts.helpers.labels import labels
from utils.helpers import get_root_ad
from utils.helpers import shape
## ************************** IO ********************************** ##
IMG_ID = 0
LABEL_ID = 1
OUTPUT_TYPES = [tf.float32, tf.int32, tf.float32]#, tf.string, tf.string] # img, label, valid_mask
ENABLE_DATA_AUGMENTATION = False
LICENSE_PLATE_TRAIN_ID = -1
BG_TRAIN_ID = 255
VOID_TRAIN_ID = 19
VOID_COLOR = [0, 0, 0]

def get_colormap(label):
  _, LUT = get_labels_color_palette()
  cm_label = tf.nn.embedding_lookup(LUT, tf.argmax(label, axis=-1)).numpy().astype(np.uint8)
  return cm_label

def get_labels_color_palette():
  palette = [(label.name, list(label.color)) for label in labels if not label.trainId == 255 and not label.trainId == -1]
  void_label = ('void', VOID_COLOR)
  palette.append(void_label)
  names, colors = [entry[0] for entry in palette], [entry[1] for entry in palette]
  return np.array(names), np.array(colors)

def get_num_classes():
  names,_ = get_labels_color_palette()
  return len(list(names))

class CityscapesDataset():
    def __init__(self, params, stage, shuffle=False, cache=False):
        batchsize = params['batch_size']
        datasetfile = path.join(params['dataset_basedir'], f"{stage}.dataset")
        with open(datasetfile) as f:
            filepath_lines = f.readlines()
        self.base_datadir = params['dataset_basedir']
        dataset = tf.data.Dataset.from_tensor_slices(filepath_lines)
        dataset = dataset.map(lambda x: tuple(tf.py_function(func=self.process_path, inp=[x],
                                                             Tout=OUTPUT_TYPES)),
                              num_parallel_calls=tf.data.experimental.AUTOTUNE)
        if cache:
          dataset = dataset.cache() # donot use if file is large
        if shuffle:
          dataset = dataset.shuffle(1000)
        # dataset = dataset.repeat(params['epoch'])
        dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)
        dataset = dataset.batch(batchsize, drop_remainder=True)
        self.channels = params['channels']
        self.shape = shape(height=params['img_height'], width=params['img_width'])
        self.new_shape = shape(height=params['new_height'], width=params['new_width'])
        self.num_sample = len(filepath_lines)
        self.dataset = dataset
        self.batchsize = batchsize
        self.iters = int(len(filepath_lines)/batchsize)
        self.data_format = params['data_format']

    def read_imgs(self, file_path):
      file_path = tf.strings.join([self.base_datadir, file_path], path.sep)
      imgs = tf.io.read_file(file_path)
      imgs = tf.image.decode_png(imgs, channels=3)
      imgs = tf.image.resize(imgs, [self.new_shape.height, self.new_shape.width])
      # imgs = tf.image.rgb_to_grayscale(imgs)
      imgs = tf.cast(imgs, OUTPUT_TYPES[IMG_ID])
      imgs = tf.math.divide(imgs, 255 * tf.ones(tf.shape(imgs), dtype=imgs.dtype))
      # imgs = imgs / 255.0
      imgs = tf.image.convert_image_dtype(imgs, OUTPUT_TYPES[IMG_ID])
      if ENABLE_DATA_AUGMENTATION:
        imgs = tf.image.random_flip_left_right(imgs)
        imgs = tf.image.random_flip_up_down(imgs)
        imgs = tf.image.random_brightness(imgs, 0.3)
      return imgs

    def read_labels_and_generate_mask(self, file_path):
      file_path = tf.strings.join([self.base_datadir, file_path], path.sep)
      # labels = tf.convert_to_tensor(np.array(Image.open(file_path.numpy())))
      # labels = tf.image.resize(tf.expand_dims(labels, axis=-1), [self.new_shape.height, self.new_shape.width])
      # labels = tf.squeeze(labels)
      labels = tf.convert_to_tensor(np.array(Image.open(file_path.numpy())))
      void_id_mask = tf.logical_or(labels == BG_TRAIN_ID, labels == LICENSE_PLATE_TRAIN_ID)
      labels = tf.where(void_id_mask, tf.constant(VOID_TRAIN_ID, dtype=labels.dtype), labels)
      labels = tf.one_hot(labels, depth=get_num_classes())
      labels = tf.image.resize(labels, [self.new_shape.height, self.new_shape.width])
      labels = tf.where(labels == 0, tf.constant(0, dtype=labels.dtype), tf.constant(1, dtype=labels.dtype))
      valid_mask = labels[...,get_num_classes()-1] == 0
      labels = tf.cast(labels, OUTPUT_TYPES[LABEL_ID])

      return labels, valid_mask

    def process_path(self, file_path):
      img_label_path = tf.strings.strip(tf.strings.split(file_path, " "))
      imgs = self.read_imgs(img_label_path[IMG_ID])
      labels, valid_mask = self.read_labels_and_generate_mask(img_label_path[LABEL_ID])
      # valid_mask = tf.math.logical_not(tf.logical_or(labels == BG_TRAIN_ID, labels == LICENSE_PLATE_TRAIN_ID))
      # labels = tf.where(valid_mask, labels, VOID_TRAIN_ID)
      return imgs, labels, tf.cast(valid_mask, dtype=tf.float32)#, img_label_path[IMG_ID], img_label_path[LABEL_ID]


def benchmark(ds, num_epochs=2):
    start_time = time.perf_counter()
    for epoch_num in range(num_epochs):
        for img, label, invmask in tqdm(ds.dataset, total=ds.num_sample, disable=True, ncols=80):
        #for img, label in ds.dataset:
          # Performing a training step
          print(label)
          for i in range(batch_size):
            plt.subplot(3, batch_size, i+1)
            plt.imshow(img[i,...].numpy())
            plt.subplot(3, batch_size, (batch_size)+i+1)
            plt.imshow(label[i,...].numpy())
            plt.subplot(3, batch_size, (2*batch_size)+i+1)
            plt.imshow(invmask[i,...].numpy())
          plt.show()
    tf.print("Execution time:", time.perf_counter() - start_time)


def create_color_palette():
    names, colors = get_labels_color_palette()
    PALETTE_HEIGHT = 400
    PALETTE_WIDTH = 100
    STRIP_HEIGHT = 20
    palette = np.zeros((PALETTE_HEIGHT, PALETTE_WIDTH, 3), np.int16)
    ones = np.ones((STRIP_HEIGHT, PALETTE_WIDTH), np.int16)
    for i in range(0, int(PALETTE_HEIGHT/STRIP_HEIGHT)):
        img = Image.new('RGB', (PALETTE_WIDTH, STRIP_HEIGHT), color=tuple(colors[i]))
        d = ImageDraw.Draw(img)
        d.text((5,5), names[i][:], fill=(128, 128, 128))
        strip =  np.asarray(img)
        palette[i*STRIP_HEIGHT:i*STRIP_HEIGHT+STRIP_HEIGHT, :, :] = strip
    palette_img = Image.fromarray(np.uint8(palette))
    palette_img.save(path.join(get_root_ad(), 'dev', 'data', 'cityscapes_color_palette.png'))
    print(f"Palette saved at {path.join(get_root_ad(), 'dev', 'data', 'cityscapes_color_palette.png')}")
    return palette

""" num_epochs = 2
batch_size = 3
train_ds = CityscapesDataset(batch_size, num_epochs,
            '/media/dawudmaxx/ACTIVE/DEV/05_Data/cityscapes/train.dataset')
benchmark(train_ds, num_epochs) """


""" save color palettes
palette = create_color_palette()
plt.imshow(palette)
plt.show()
"""