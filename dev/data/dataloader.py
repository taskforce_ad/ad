import tensorflow as tf
import os.path as path
import time

from utils.io import read_color_image, read_label_image
                                
class ArtificialDataset(tf.data.Dataset):
    def _generator(num_samples):
        # Opening the file
        time.sleep(0.03)

        for sample_idx in range(num_samples):
            # Reading data (line, record) from the file
            time.sleep(0.015)

            yield (sample_idx,)

    def __new__(cls, num_samples=3):
        return tf.data.Dataset.from_generator(
            cls._generator,
            output_types=tf.dtypes.int64,
            output_shapes=(1,),
            args=(num_samples,)
        )

def mapped_function(s):
    # Do some hard pre-processing
    tf.py_function(lambda: time.sleep(0.03), [], ())
    return s
        
def benchmark(dataset, num_epochs=2):
    start_time = time.perf_counter()
    for epoch_num in range(num_epochs):
        for sample in dataset:
            # Performing a training step
            time.sleep(0.01)
    tf.print("Execution time:", time.perf_counter() - start_time)
    

##################### bencmark #######################
# The naive approach
tf.print("The naive approach")
benchmark(ArtificialDataset())

# Prefetching
tf.print("Prefetching")
benchmark(
    ArtificialDataset()
    .prefetch(tf.data.experimental.AUTOTUNE)
)

# Sequential interleave
tf.print("Sequential interleave")
benchmark(
    tf.data.Dataset.range(2)
    .interleave(ArtificialDataset)
)

# Parallel interleave
tf.print("Parallel interleave")
benchmark(
    tf.data.Dataset.range(2).
    interleave(ArtificialDataset, 
               num_parallel_calls=tf.data.experimental.AUTOTUNE)
)

# Sequential mapping
tf.print("Sequential mapping")
benchmark(
    ArtificialDataset()
    .map(mapped_function)
)

# parallel mapping
tf.print("parallel mapping")
benchmark(
    ArtificialDataset().map(mapped_function,num_parallel_calls=tf.data.experimental.AUTOTUNE)
)

# caching # Apply time consuming operations before cache
tf.print("caching")
benchmark(ArtificialDataset().map(mapped_function).cache(), 5)