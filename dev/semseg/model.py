# cityscapes training model
# train IDs: 20 [0-19, -1]
# IDs: 35 [0-33, -1]
# 1 void class 19 valid classes
import tensorflow as tf
import tensorflow.keras as keras
# from tensorflow_examples.models.pix2pix import pix2pix

def get_cityscapes_model(shape):
    model = keras.models.Sequential()
    initializer = tf.keras.initializers.GlorotUniform()
    # initializer = None
    model.add(keras.layers.Input(shape=shape))
    # encoder
    model.add(keras.layers.Conv2D(64, 3, activation='relu', padding='same', strides=2, kernel_initializer=initializer))
    model.add(keras.layers.Conv2D(128, 3, activation='relu', padding='same', strides=2, kernel_initializer=initializer))
    model.add(keras.layers.Conv2D(256, 3, activation='relu', padding='same', strides=2, kernel_initializer=initializer))
    model.add(keras.layers.Conv2D(1024, 3, activation='relu', padding='same', strides=2, kernel_initializer=initializer))
    model.add(keras.layers.Conv2D(2048, 3, activation='relu', padding='same', strides=2, kernel_initializer=initializer))
    # decoder
    model.add(keras.layers.Conv2DTranspose(2048, kernel_size=3, strides=2, activation='relu', padding='same', kernel_initializer=initializer))
    model.add(keras.layers.Conv2DTranspose(1024, kernel_size=3, strides=2, activation='relu', padding='same', kernel_initializer=initializer))
    model.add(keras.layers.Conv2DTranspose(256, kernel_size=3, strides=2, activation='relu', padding='same', kernel_initializer=initializer))
    model.add(keras.layers.Conv2DTranspose(128, kernel_size=3, strides=2, activation='relu', padding='same', kernel_initializer=initializer))
    model.add(keras.layers.Conv2DTranspose(64, kernel_size=3, strides=2, activation='relu', padding='same', kernel_initializer=initializer))
    model.add(keras.layers.Conv2D(20, kernel_size=(3,3), padding='same', kernel_initializer=initializer))
    return model

def mnist_semseg_model(shape):
    # https://awaywithideas.com/a-simple-example-of-semantic-segmentation-with-tensorflow-keras/
    model = keras.models.Sequential()
    layers = keras.layers
    model.add(layers.Conv2D(filters=16, kernel_size=(3, 3), activation='relu', input_shape=shape, padding='same'))
    model.add(layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same'))
    model.add(layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same'))
    model.add(layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same'))
    model.add(layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same'))
    model.add(layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same'))
    model.add(layers.UpSampling2D(size=(2, 2)))
    model.add(layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same'))
    model.add(layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same'))
    model.add(layers.UpSampling2D(size=(2, 2)))
    model.add(layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same'))
    model.add(layers.Conv2D(filters=16, kernel_size=(3, 3), activation='relu', padding='same'))
    model.add(layers.Conv2D(filters=20, kernel_size=(3, 3), activation='sigmoid', padding='same'))
    return model

# def unet_semseg(shape):
#     base_model = tf.keras.applications.MobileNetV2(input_shape=shape, include_top=False)

#     # Use the activations of these layers
#     layer_names = [
#         'block_1_expand_relu',   # 64x64
#         'block_3_expand_relu',   # 32x32
#         'block_6_expand_relu',   # 16x16
#         'block_13_expand_relu',  # 8x8
#         'block_16_project',      # 4x4
#     ]
#     base_model_outputs = [base_model.get_layer(name).output for name in layer_names]

#     # Create the feature extraction model
#     down_stack = tf.keras.Model(inputs=base_model.input, outputs=base_model_outputs)

#     down_stack.trainable = False

#     up_stack = [
#         pix2pix.upsample(512, 3),  # 4x4 -> 8x8
#         pix2pix.upsample(256, 3),  # 8x8 -> 16x16
#         pix2pix.upsample(128, 3),  # 16x16 -> 32x32
#         pix2pix.upsample(64, 3),   # 32x32 -> 64x64
#     ]

#     # unet
#     inputs = tf.keras.layers.Input(shape=shape)

#     # Downsampling through the model
#     skips = down_stack(inputs)
#     x = skips[-1]
#     skips = reversed(skips[:-1])

#     # Upsampling and establishing the skip connections
#     for up, skip in zip(up_stack, skips):
#         x = up(x)
#         concat = tf.keras.layers.Concatenate()
#         x = concat([x, skip])

#     # This is the last layer of the model
#     last = tf.keras.layers.Conv2DTranspose(filters=20, kernel_size=3, strides=2,padding='same')  #64x64 -> 128x128

#     x = last(x)

#     return tf.keras.Model(inputs=inputs, outputs=x)
