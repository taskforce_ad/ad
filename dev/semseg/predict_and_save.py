import tensorflow as tf
from tqdm import tqdm
import os
from parameters import base_parameters as params
from utils.helpers import ModelManager, get_model_from_path
from train_helpers import load_dataset_cityscapes, get_colormaps

model_unet_name = "unet_epoch-1_02"

modelmanager = ModelManager(__file__, params['output_path'], 'test')
# model = get_model_from_path(os.path.join(modelmanager.outputdir_path, model_unet_name))
model = get_model_from_path(os.path.join(modelmanager.checkpoint_path, params['model_name']))
_, _, test_ds = load_dataset_cityscapes(batchsize=1)
# model.summary()

count = 0
for samples in tqdm(test_ds.dataset, total=test_ds.iters, ncols=80):
    images, targets, valid = samples[0], samples[1], samples[2]
    predictions = tf.argmax(model(images, training=False), axis=-1)
    _, predictions_cm = get_colormaps(targets, predictions)

    # predictions_dir = os.path.join(modelmanager.outputdir_path, "predictions", model_unet_name)
    predictions_dir = os.path.join(modelmanager.outputdir_path, "predictions", "tmp")
    os.makedirs(predictions_dir, exist_ok=True)
    for i in range(test_ds.batchsize):
        # save image
        predict_img = predictions_cm[i,...]
        tf.compat.v1.keras.preprocessing.image.save_img(os.path.join(predictions_dir, f"predict_{count}.png"), predict_img)
        count = count + 1

print("Prediction on Test Set complete")