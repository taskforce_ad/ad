import tensorflow as tf
from tqdm import tqdm
import time

from parameters import base_parameters as params
from dev.data.cityscapes_dataset import get_num_classes
from utils.helpers import ModelManager
from train_helpers import load_dataset_cityscapes, load_model, create_histograms, train_step_returns, clean_and_setup

@tf.function
def train_step(samples, model, optimizer, summary_writer, step):
    images, targets, valid_mask = samples[0], samples[1], samples[2]
    with tf.GradientTape() as tape:
        predictions = model(images)
        # target labels are one hot encoded
        loss_default = tf.keras.losses.categorical_crossentropy(targets, predictions, from_logits=True)
        loss = tf.math.multiply(loss_default, valid_mask)
    gradient = tape.gradient(loss, model.trainable_variables)
    optimizer.apply_gradients(zip(gradient, model.trainable_variables))
    return train_step_returns(predictions, loss, gradient)

@tf.function
def eval_step(samples, model, categorical_accuracy):
    images, targets, valid = samples[0], samples[1], samples[2]
    predictions = model(images, training=False)
    categorical_accuracy.update_state(targets, predictions)

# prepare
clean_and_setup()
train_ds, val_ds, _ = load_dataset_cityscapes()
model, optimizer = load_model()
model.summary()
print(params)

# training and evaluation
modelmanager = ModelManager(__file__, params['output_path'], 'train')
summary_writer = modelmanager.get_summary_writer()
accuracy = tf.keras.metrics.Accuracy()
categorical_accuracy = tf.keras.metrics.CategoricalAccuracy()
miou = tf.keras.metrics.MeanIoU(get_num_classes())
step = 0
epoch_num = 0
while epoch_num < params['epoch']:
    count = 0
    if (step == 0 and modelmanager.resume(params['model_name'])):
        epoch_num = modelmanager.get_epoch_num()
        model = modelmanager.get_saved_model(params['model_name'])
        print(f"[EPOCH] Resumed from Epoch: [{epoch_num}/{params['epoch']}]")
        epoch_num += 1
    print(f"[EPOCH] Epoch: [{epoch_num}/{params['epoch']}]")

    start_time = time.perf_counter()
    print('------------------- TRAINING ---------------------')
    loss = 0.0
    for samples in tqdm(train_ds.dataset, total=train_ds.iters, ncols=80):
        # hack
        if (count == len(train_ds.dataset)):
            break
        train_returns = train_step(samples, model, optimizer, summary_writer, step)
        loss += train_returns.loss
        # debug
        # if ((train_ds.batchsize * epoch_num) + step) % params['logging_frequency'] == 0:
        create_histograms(modelmanager, train_returns, samples, count)
        count = count + params['batch_size']
        summary_writer.flush()
        # if (train_ds.batchsize * step) % params['pre_epoch_model_saving_frequency'] == 0:
        #     modelmanager.save_pre_epoch_model(model, params['model_name'], epoch_num, step)
        step += 1
    loss = tf.reduce_mean(loss)
    print(f"[EPOCH] Loss: {loss}")

    print('------------------- EVALUATION -------------------')
    for samples in tqdm(val_ds.dataset, total=val_ds.iters, ncols=80):
        eval_step(samples, model, categorical_accuracy)
        final_c_accuracy = tf.reduce_sum(categorical_accuracy.result())
    print(f"[EPOCH] Eval Categorical Accuracy: {final_c_accuracy}")
    categorical_accuracy.reset_states()

    loss_and_accuracy = {'loss': loss, 'categorical_accuracy':final_c_accuracy}
    do_logging = modelmanager.save_model(model, params['model_name'], epoch_num, loss_and_accuracy)
    # if (do_logging):
    #     create_histograms(train_returns, samples, step)
    print(f"[EPOCH] Execution time for epoch:: {time.perf_counter() - start_time}")
    epoch_num += 1