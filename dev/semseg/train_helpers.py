from os.path import join
from os import remove
import tensorflow as tf
import numpy as np
from matplotlib import pyplot as plt
import glob
from collections import namedtuple

from parameters import base_parameters as params
from unet import UNetCompiled
from dev.data.cityscapes_dataset import CityscapesDataset, get_labels_color_palette
from utils.helpers import get_output_dir

train_step_returns = namedtuple('train_step_returns', ['predictions', 'loss', 'gradient'])

def load_dataset_cityscapes(batchsize=None):
    if (not batchsize == None):
        params['batch_size'] = batchsize
    train_ds = CityscapesDataset(params, 'train')
    val_ds = CityscapesDataset(params, 'val')
    test_ds = CityscapesDataset(params, 'test')
    return train_ds, val_ds, test_ds

def load_model():
    # model = get_cityscapes_model((params['new_height'], params['new_width'], params['channels']))
    # model = mnist_semseg_model((params['new_height'], params['new_width'], params['channels']))
    # model = unet_semseg((params['new_height'], params['new_width'], params['channels']))
    model = UNetCompiled((params['new_height'], params['new_width'], params['channels']))
    optimizer = tf.keras.optimizers.Adam(learning_rate=params['learning_rate'])
    model.build((None, params['new_width'], params['new_height'], params['channels']))
    return model, optimizer

def get_colormaps(targets, predictions):
    # predictions = tf.cast(predictions, tf.float32)
    # CAR_TRAIN_ID = 13
    # LUT = np.array([[0,0,0], [255,0,0]])
    _, LUT = get_labels_color_palette()

    # targets = tf.where(targets == CAR_TRAIN_ID, tf.constant(1), tf.constant(0))
    # predictions = tf.where(predictions == CAR_TRAIN_ID, tf.constant(1), tf.constant(0))

    colormap_targets = tf.nn.embedding_lookup(LUT, targets)
    colormap_predictions = tf.nn.embedding_lookup(LUT, predictions)
    return colormap_targets, colormap_predictions

def create_histograms(manager, train_returns: train_step_returns, samples, count):
    predict_img = tf.argmax(train_returns.predictions, axis=-1)
    loss = train_returns.loss
    gradient = train_returns.gradient
    epochs, losses, accuracies = manager.get_losses_and_accuracies(params['model_name'])

    input_img, label_img, valid_mask = samples[0], samples[1], samples[2]
    fig = plt.figure(figsize=(10, 10)) # width, height in inches
    cm_label, cm_predict = get_colormaps(tf.argmax(label_img, axis=-1), predict_img)
    batch_size = params['batch_size']

    index = 0
    for row in range(batch_size):
        rows = 3*batch_size
        cols = 2

        index += 1
        fig.add_subplot(rows, cols, index)
        plt.ylabel("Label")
        plt.imshow(cm_label[row,...])
        index += 1
        fig.add_subplot(rows, cols, index)
        plt.ylabel("Loss")
        # plt.imshow(loss_norm)
        plt.gca().invert_yaxis()
        plt.pcolormesh(loss[row,...], cmap='inferno')
        plt.colorbar()

        index += 1
        fig.add_subplot(rows, cols, index)
        plt.ylabel("Image")
        plt.imshow(input_img[row,...])
        index += 1
        ax1 = fig.add_subplot(rows, cols, index)
        ax2 = ax1.twinx()
        ax1.set_xlabel('Epoch')
        ax1.set_ylabel('Categorical Accuracy', color='g')
        ax2.set_ylabel('Loss', color='b')
        ax1.plot(epochs, accuracies, 'g-')
        ax2.plot(epochs, losses, 'b-')
        # Setting Y limits
        # ax2.set_ylim(0, 35)
        # ax.set_ylim(-20, 100)
        # plt.show()


        index += 1
        fig.add_subplot(rows, cols, index)
        plt.ylabel("Prediction")
        plt.imshow(cm_predict[row,...])
        index += 1
        fig.add_subplot(rows, cols, index)
        flat_array = predict_img[row,...].numpy().flatten()
        unique, counts = np.unique(flat_array, return_counts=True)
        plt.plot(unique, counts)

        # index += 1
        # fig.add_subplot(rows, cols, index)
        # plt.ylabel("Loss")
        # # plt.imshow(loss_norm)
        # plt.gca().invert_yaxis()
        # plt.pcolormesh(loss[row,...], cmap='inferno')
        # plt.colorbar()
        # index += 1
        # fig.add_subplot(rows, cols, index)
        # flat_array = loss[row,...].numpy().flatten()
        # unique, counts = np.unique(flat_array, return_counts=True)
        # plt.plot(unique, counts)

        # index += 1
        # fig.add_subplot(rows, cols, index)
        # plt.ylabel("Gradient")
        # plt.gca().invert_yaxis()
        # plt.pcolormesh(gradient[row,...], cmap='inferno')
        # plt.colorbar()
        # index += 1
        # fig.add_subplot(rows, cols, index)
        # flat_array = gradient[row,...].numpy().flatten()
        # unique, counts = np.unique(flat_array, return_counts=True)
        # plt.plot(unique, counts)

    plt.savefig(join(get_output_dir(__file__, params['output_path']), f"target_predict_{count}.png"))
    plt.close(fig)

def print_stats(images):
    tf.print(f"tf_reduce images: {tf.reduce_mean(tf.slice(images, [0, 0, 0, 0], [1,10,10,1]))}")

def print_weights(model):
    W = []
    B = []
    for layer in model.layers:
        W.append(np.mean(layer.get_weights()[0]))
        B.append(np.mean(layer.get_weights()[1]))
    print(f"[INFO] W mean: {np.mean(np.array(W))}")
    print(f"[INFO] B mean: {np.mean(np.array(B))}")

def use_gpu():
  device_name = tf.test.gpu_device_name()
  if (device_name != '/device:GPU:0'):
    raise SystemError('GPU device not found')
  print('Found GPU at: {}'.format(device_name))

def clean_events():
    outdir_path = get_output_dir(__file__, params['output_path'])
    filelist = glob.glob(join(outdir_path, '*', 'events.*'))
    for file in filelist:
        remove(file)
    
def clean_and_setup():
    # clean tf.events
    clean_events();
    # ensure gpu usage first
    use_gpu()
