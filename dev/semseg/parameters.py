base_parameters = {
    'dataset_basedir' : '/workspace/data/test_data/semseg/cityscapes',
    'optimizer': 'adam',
    'learning_rate': 0.0001, # epoch0-1(0.0001), epoch2-6(0.000001), epoch6->(0.00000001)
    'data_format': 'channels_last',
    'epoch': 2000,
    'batch_size': 2,
    'img_height': 1024,
    'img_width' : 2048,
    'new_height': 512,
    'new_width': 1024,
    'channels' : 3,
    'logging_frequency': 300,
    'pre_epoch_model_saving_frequency': 500,
    'output_path': 'outputs',
    'model_name': 'model-semseg'
}
