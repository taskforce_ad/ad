import tensorflow as tf
import numpy as np
from tqdm import tqdm
from tensorflow.keras.losses import categorical_crossentropy

from parameters import base_parameters as params
from train_helpers import save_model
from utils.helpers import load_mnist_dataset
from utils.gpu_init import init_session

# init_session(visible_device=0, memory=6)
class TestModel(tf.keras.models.Model):
    def __init__(self):
        super(TestModel, self).__init__()
        self.conv = tf.keras.layers.Conv2D(64, 3, activation='relu')
        self.flatten = tf.keras.layers.Flatten()
        self.dense = tf.keras.layers.Dense(10, activation='softmax')
    def build(self, input_shape):
        super(TestModel, self).build(input_shape)
    def call(self, inputs):
        net = self.conv(inputs)
        net = self.flatten(net)
        net = self.dense(net)
        return net

@tf.function
def train_step(X, Y, model, optimizer):
    with tf.GradientTape() as tape:
        Y_predict = model(X)
        loss = categorical_crossentropy(Y, Y_predict)
    gradient = tape.gradient(loss, model.variables)
    optimizer.apply_gradients(zip(gradient, model.variables))
    return tf.reduce_mean(loss)
    
# load dataset
x_train, y_train, x_test, y_test, num_train, num_test = load_mnist_dataset()
    
# load model
model = TestModel()
model(tf.zeros((1,28,28,1)))
optimizer = tf.keras.optimizers.Adam(learning_rate=0.01)


# training
iterations = int(num_train / params['batch_size'])
for epoch in range(1, params['epoch']+1):
    loss_epoch = 0.0
    print(f"+++++++++++++++ Epoch {epoch}/{params['epoch']} ++++++++++++++++++++")
    for i in tqdm(range(0, num_train, params['batch_size']), total=iterations, ncols=80):    
        first = i
        last = first + params['batch_size']
        if last > num_train:
            last = num_train        
        loss_epoch += train_step(x_train[first:last, ...], y_train[first:last, ...], model, optimizer)
    print(f"loss:{loss_epoch}")
    # model.evaluate(x_test, y_test)    
    save_model(model)    
