from os import mkdir
from os.path import exists, abspath, dirname, join
import tensorflow as tf

import numpy as np
import os
import PIL
import PIL.Image

from parameters import base_parameters as params

def load_data(imgs, labels1, range_color=[0,255], range_label=[0,21]):
    # load data parallely
    # image
    image = tf.keras.preprocessing.image.load_img(imgs, color_mode='grayscale')
    image_arr = tf.keras.preprocessing.image.img_to_array(image)
    image_arr = image_arr / range_color[1]

    # labels1
    label1 = tf.keras.preprocessing.image.load_img(labels1, color_mode='grayscale')
    label_arr1 = tf.keras.preprocessing.image.img_to_array(label1)
    label_arr1 = label_arr1.astype(np.uint8)

    return image_arr, label_arr1

def save_model(model):
    basepath = dirname(__file__)
    modeldir = join(basepath, params['output_path'])
    if not exists(modeldir):
        mkdir(modeldir)
    filepath = join(modeldir, params['model_name'])
    tf.keras.models.save_model(model, filepath)
    print(f"NN Model:{params['model_name']} saved")

# load image
if False:
    datadir = "/mnt/d/DEV/05_Data/cityscapes/wsl-minimal"
    dataset = open(join(datadir, 'train.dataset'), 'r')
    samples = dataset.readlines()
    num_sample = len(samples)
    for sample in samples:
        paths = sample.split(' ')
        color_path = paths[0].rstrip()
        gtfine_path = paths[1].rstrip()
        color, label= load_data(color_path, gtfine_path)