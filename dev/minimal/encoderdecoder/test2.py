## AIM
## Design VGG encoder-decoder network
import tensorflow as tf
import numpy as np
from tensorflow.keras.layers import Conv2D, Flatten, Dense, MaxPool2D
from tensorflow.keras.optimizers import Adam
    
class VGGBlock(tf.keras.layers.Layer):
    def __init__(self, features, maxpool=False):
        super(VGGBlock, self).__init__()
        self.maxpool = maxpool
        self.features = features
    def build(self, input_shape):
        super(VGGBlock, self).build(input_shape)
    def call(self, x):
        if self.maxpool:
            x = MaxPool2D((2,2))(x)
        x = Conv2D(self.features, (3,3), padding='same', activation='relu')(x)
        x = Conv2D(self.features, (3,3), padding='same', activation='relu')(x)
        x = Conv2D(self.features, (3,3), padding='same', activation='relu')(x)
        return x 

class VGGEncoder(tf.keras.models.Model):
    def __init__(self):
        super(VGGEncoder, self).__init__()
    # def build(self, input_shape):
    #     super(VGGEncoder, self).build(input_shape)
    def call(self, x):
        x = VGGBlock(32, True)(x)
        x = VGGBlock(64, True)(x)
        x = VGGBlock(128, False)(x)
        x = VGGBlock(256, True)(x)
        x = VGGBlock(512, False)(x)
        x = VGGBlock(1024, True)(x)
        x = VGGBlock(2048, True)(x)
        x = Flatten()(x)
        x = Dense(10, activation='softmax')(x)
        return x

def get_vgg_model():
    model = VGGEncoder()
    # model.build((1,224,224,3))
    optimizer = Adam(learning_rate=0.01)
    model.compile(optimizer=optimizer, loss='categorical_crossentropy', 
                  metrics=['accuracy'])
    return model, optimizer

model, _ = get_vgg_model()
# set input size
model(tf.zeros((1, 224, 224, 3)))


batch_size = 100
fm = model(tf.zeros((batch_size, 224, 224, 3)))
print(fm.shape)