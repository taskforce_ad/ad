from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Dense, Flatten, Dropout
from tensorflow.keras.optimizers import Adam

from utils.helpers import load_mnist_dataset, get_root_ad

# global params
kDataFormat = 'channels_last'
kEpoch = 1
kBatchSize = 100

# create model
model = Sequential()
model.add(Conv2D(64, (3,3), input_shape=(28,28, 1), data_format=kDataFormat, activation='relu'))
model.add(Flatten())
model.add(Dense(10, activation='softmax'))
optimizer = Adam(learning_rate=0.01)
model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])

# load mnist data
x_train, y_train, x_test, y_test = load_mnist_dataset()
print(f"TRAIN data loaded x:dtype:{x_train.dtype}{x_train.shape}, y:dtype:{y_train.dtype}{y_train.shape}")
print(f"TEST data loaded x:dtype:{x_test.dtype}{x_test.shape}, y:dtype:{y_test.dtype}{y_test.shape}")

# model fit
model.fit(x_train, y_train, batch_size=kBatchSize, epochs=kEpoch, validation_data=(x_test, y_test))

# print path ad
print(get_root_ad())