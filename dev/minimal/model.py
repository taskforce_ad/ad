import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, Conv2DTranspose, MaxPooling2D, Dense, Flatten
from tensorflow.keras.optimizers import Adam

from parameters import base_parameters as params
    
def get_model_optimizer():
    model = Sequential()
    model.add(Conv2D(64, (3,3), input_shape=(28,28,1), activation='relu'))
    model.add(MaxPooling2D((2,2)))
    model.add(Flatten())
    model.add(Dense(10, activation='softmax'))
    
    if params['optimizer'] == 'adam':
        optimizer = Adam(learning_rate=params['learning_rate'])
    model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])
    return model, optimizer