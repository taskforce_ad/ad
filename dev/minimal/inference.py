import tensorflow as tf
from tensorflow.keras.models import load_model
from os.path import exists, dirname, join
from parameters import base_parameters as params

from utils.helpers import load_mnist_dataset

modeldir = join(dirname(__file__), params['output_path'])
# load model
if exists(modeldir):
    # load model
    modelpath = join(modeldir, params['model_name'])
    model = load_model(modelpath)
    # load dataset
    _, _, x_test, y_test, _, _ = load_mnist_dataset()
    model.evaluate(x_test, y_test)
    
else:
    print("Failed to load model")    