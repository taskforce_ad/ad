base_parameters = {
    'optimizer': 'adam',
    'learning_rate': 0.01,
    'data_format': 'channels_last',
    'epoch': 50,
    'batch_size': 1500,
    'output_path': 'outputs',
    'model_name': 'model-minimal'
}