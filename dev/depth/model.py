__copyright__="""
All rights are reserved to Tskforce (AD).
Any form of reuse, distribution and modifiation without prior permission is considered as a legal offence.
"""

import os
import numpy as np
# tensorflow and keras specific
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import Model
from tensorflow.keras.utils import plot_model
from tensorflow.keras.layers import InputLayer, Layer, Conv2D, MaxPool2D
# image specific
from tensorflow.keras.preprocessing.image import load_img, img_to_array
from tensorflow.keras.applications.inception_v3 import preprocess_input

'''
## BUG FIX: plot_model(..) not working
1. change "os.errno" to "errno" in <VENV_ROOT>\Lib\site-packages\pydot.py
   1. add "import errno"
   2. change "os.errno" to "errno" in <VENV_ROOT>\Lib\site-packages\pydot.py
2. install [graphviz](http://www.graphviz.org/download/) from here
3. set the <GRAPHVIZ_ROOT>\bin to the PATH variable OR add it in python file
4. re-start the Python IDE
'''
# os.environ["PATH"] += os.pathsep + 'C:\Program Files (x86)\Graphviz2.38\bin'
class NN(Layer):
    def __init__(self, dataformat="channels_last"):
        super(NN, self).__init__()
        self.dataformat = dataformat

    def build(self, inputshape):
        super(NN, self).build(inputshape)
        model = load_pretrained_inception_net_encoder(input_shape=inputshape)
        outputs = [model.get_layer("mixed0").output,model.get_layer("mixed3").output,model.get_layer("mixed8").output]
        self.disp0 = Model(inputs=model.input, outputs=outputs, name="disp0")
    def call(self, input):		
        disp0 = self.disp0(input)
        return disp0
        
def load_pretrained_inception_net_encoder(input_shape, freeze_all=False):
    model_inceptionV3 = keras.applications.inception_v3.InceptionV3(include_top=False, 
                                                                    weights='imagenet', 
                                                                    input_tensor=None, 
                                                                    input_shape=input_shape,
                                                                    pooling=None)
    model_inceptionV3.trainable = freeze_all
    # create a keras Model from the encoder part
    return model_inceptionV3
        
class Encoder(Layer):
    def __init__(self):
        super(Encoder, self).__init__()
        conv = Conv2D(128, kernel=3, stride=1, padding='same', data_format='channels_last')
        maxpool = MaxPool2D(3, 2, padding='same')

    def build(self, inputshape):
        super(Encoder, self, inputshape).build()
        
    def call(self, input):
        net = conv(input)
        net = maxpool(net)
        net = conv(net)
        net = maxpool(net)
        return net
    
def test():    
    # main
    model = load_pretrained_inception_net_encoder(input_shape=(400, 640, 3))
    #model.summary()
    
    # save the model graph
    plot_model(model, to_file='model.png')
    # dummy dataset    
    image = np.expand_dims(img_to_array(load_img('model.png', target_size=(400, 640))), axis=0)
    # dataset = tf.data.Dataset.from_tensor_slices(img)
    
    '''
    FINDING
    1. moxied0 - mixed10 are 11 different concat layers
    '''
    # scale1 scale1/2 scale1/4 scale1/8. Choosing first 4 scaled outputs
    layers = ['mixed0', 'mixed1', 'mixed2', 'mixed3']
    layers_new_name = ['disp1', 'disp2', 'disp4', 'net']
    # create model
    net = Model(inputs=model.input, outputs=model.get_layer(layers[-1]).output, name=layers_new_name[-1])
    features = net.predict(image)
    features.shape
    
    disp1 = net.get_layer(layers[0]).output
    disp2 = net.get_layer(layers[1]).output
    disp4 = net.get_layer(layers[2]).output
    # models = [Model(model.inputs, model.get_layer(layers[x]).output, layers_new_name[x]) for x in range(len(layers))]
