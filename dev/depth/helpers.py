from os.path import abspath, dirname, sep
import sys
def init_module():
    from os.path import abspath, dirname, sep
    import sys
    dir = abspath(dirname(__file__))
    ROOT_PATH = dir[:dir.rfind(sep+'ad'+sep)]
    try:    
        sys.path.index(ROOT_PATH)
    except ValueError:
        sys.path.insert(0, ROOT_PATH)
def get_ad_root():
    dir = abspath(dirname(__file__))
    return dir[:dir.rfind(sep+'ad'+sep)]
#########################################################################################

import cv2
import numpy as np
import os

class LayerInfo():
    def __init__(self, model):
        self.model = model
        self.layer_infos = [{"name":layer.name,"input":layer.input, "output":layer.output} for layer in model.layers]
        self.num_layers = len(model.layers)
    def get_index(self, name):
        for index in range(self.num_layers):
            if self.layer_infos[index]["name"] == name:
                return index
        assert(False, "Index not found")       
    def get_name(self, index):
        return self.layer_infos[index]["name"]
    def get_input(self, index):
        return self.layer_infos[index]["input"]
    def get_output(self, index):
        return self.layer_infos[index]["output"]
	
#img = read_color_image(os.path.join(get_root(), "model.png"))
#print(img.dtype)