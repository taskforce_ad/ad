# insert in all usecases
from helpers import init_module, get_ad_root
init_module()

# global headers
from utils.gpu_init import init_session
from utils.io import read_color_image

from os.path import dirname, abspath, join
import numpy as np

from model import NN

def run():
    sess = init_session(visible_device=0, memory=6)
    nn = NN()
    nn.build((400,640,3))
    image = read_color_image(join(dirname(__file__), "model.png"))[0:400,0:640,0:3]
    image = np.expand_dims(image, axis=0)    
    print(nn(image))
    sess.close()    
    print(get_ad_root())
run()
