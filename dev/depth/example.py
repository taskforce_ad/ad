import tensorflow as tf

import numpy as np
import os
from matplotlib import pyplot as plt
cmap = plt.cm.plasma

from tensorflow.keras import Input, Model, Sequential
from tensorflow.keras.layers import Layer, Conv2D, MaxPool2D

data = [[[[0]], [[0]], [[0]], [[1]], [[1]], [[0]], [[0]], [[0]]], 
        [[[0]], [[0]], [[0]], [[1]], [[1]], [[0]], [[0]], [[0]]],
        [[[0]], [[0]], [[0]], [[1]], [[1]], [[0]], [[0]], [[0]]],
        [[[0]], [[0]], [[0]], [[1]], [[1]], [[0]], [[0]], [[0]]],
        [[[0]], [[0]], [[0]], [[1]], [[1]], [[0]], [[0]], [[0]]],
        [[[0]], [[0]], [[0]], [[1]], [[1]], [[0]], [[0]], [[0]]],
        [[[0]], [[0]], [[0]], [[1]], [[1]], [[0]], [[0]], [[0]]],
        [[[0]], [[0]], [[0]], [[1]], [[1]], [[0]], [[0]], [[0]]]
       ]
data = np.reshape(np.asarray(data), (1,8,8,1))

weight = [
         [[[0]], [[1]], [[0]]],
         [[[0]], [[1]], [[0]]],
         [[[0]], [[1]], [[0]]]
         ]
bias = [0.0]
weights = [np.asarray(weight), np.asarray(bias)]

for r in range(data.shape[1]):
    print([data[0,r,c,0] for c in range(data.shape[2])])

## Define model
model = Sequential()
model.add(Conv2D(1,3,2,input_shape=(8,8,1)))
model.summary()

'''
input = Input(shape=(8,8,1), name='input')
output = Conv2D(1, 3, 2, padding='same', data_format='channels_last')(input)
model = Model(input, output, name="model")
model.summary()
'''

# set the weight
model.set_weights(weights)

# predition
prediction = model.predict(data)
for r in range(prediction.shape[1]):
    print([prediction[0,r,c,0] for c in range(prediction.shape[2])])


