import os
os.environ["CUDA_VISIBLE_DEVICES"] = "" # force CPU usage
from os.path import *
from PIL import Image
import numpy as np
import time
import calendar
import argparse
import tensorflow as tf
from tensorflow.keras.layers import *
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.applications import InceptionV3

from utils.init_module import INIT
from dev.data.cityscapes_dataset import get_colormap
from prod.predict_from_onnx import onnx_predict
from prod.predict_from_openvino import openvino_predict


ad_root = INIT(memory_growth=False, ensure_gpu=False).get_ad_root()
timestamp = calendar.timegm(time.gmtime())

# Load dataset
INVALID_LABEL = 255
NUM_CLASSES = 20
VOID_LABEL = NUM_CLASSES - 1
MASK_INDEX = VOID_LABEL
IMG_HEIGHT = 512
IMG_WIDTH = 1024
IMG_CHANNELS = 3
NUM_EPOCHS = 1000
BATCH_SIZE = 2
INITIAL_LR = 1e-4
DATASET_REPEAT_COUNT = 1
MODEL_NAME=f'model-semseg-{timestamp}-epoch{NUM_EPOCHS}'
L2_REGULARIZER = tf.keras.regularizers.l2(0.1)
DATASETFILE_DIR_PATH = f'{ad_root}/data/test_data/semseg/cityscapes'
# DATASETFILE_DIR_PATH = '/mnt/y/DATA/cityscapes'
RESUME_MODEL_NAME = None # 'model-semseg-1686092626-epoch68'
OVERWRITE_MODEL = False

def split_X_Y(dataset_filename, datasetdir):
  XY = []
  with open(dataset_filename) as ds_file:
    lines = ds_file.readlines()
    XY = [(join(datasetdir, line.split(" ")[0].rstrip()),
           join(datasetdir, line.split(" ")[1].rstrip())) for line in lines]
  return XY

def read_dataset(datasetdir):
  XY_train_list = split_X_Y(join(datasetdir, 'train.dataset'), datasetdir)
  XY_test_list = split_X_Y(join(datasetdir, 'test.dataset'), datasetdir)
  XY_val_list = split_X_Y(join(datasetdir, 'val.dataset'), datasetdir)
  return XY_train_list, XY_test_list, XY_val_list

def load_img(filepath):
  img = tf.image.decode_png(tf.io.read_file(filepath), channels=3)
  img = tf.image.resize(img, [IMG_HEIGHT, IMG_WIDTH])
  img = tf.cast(img, tf.uint8)
  return img

def load_and_encode_label(filepath):
  label = tf.image.decode_png(tf.io.read_file(filepath), channels=1)
  label = tf.where(label == INVALID_LABEL, tf.constant(VOID_LABEL, dtype=label.dtype), label)
  label = to_categorical(label, num_classes=NUM_CLASSES)
  label = tf.cast(tf.image.resize(label, [IMG_HEIGHT, IMG_WIDTH]), tf.bool)
  label = tf.cast(label, tf.float16)
  return label

def get_mask(y_true):
  mask = tf.logical_not(tf.cast(y_true[:,:,:,MASK_INDEX], tf.bool))
  return mask

def apply_mask(inputs, y_true, inputs_channels):
  mask = get_mask(y_true)
  mask = tf.tile(tf.expand_dims(mask, -1), [1,1,1,inputs_channels])
  inputs = tf.where(mask, inputs, tf.constant(0, dtype=inputs.dtype))
  return inputs

xy_train_paths, xy_test_paths, xy_val_paths = read_dataset(DATASETFILE_DIR_PATH)
# make val set smaller
# xy_val_paths = xy_val_paths[0:30]
print(f'[INFO] Size of train set: {len(xy_train_paths)}')
print(f'[INFO] Size of val set: {len(xy_val_paths)}')

# Create dataset
def process_path(sample):
  img_path, label_path = sample[0], sample[1]
  img = load_img(img_path)
  label = load_and_encode_label(label_path)
  return img, label
dataset = tf.data.Dataset.from_tensor_slices(xy_train_paths)
dataset = dataset.map(lambda x: tf.py_function(func=process_path,
                                               inp=[x],
                                               Tout=[tf.uint8, tf.float16]),
                                               num_parallel_calls=tf.data.experimental.AUTOTUNE)
dataset = dataset.batch(BATCH_SIZE, drop_remainder=True)
dataset = dataset.shuffle(len(xy_train_paths), reshuffle_each_iteration=True) # !! fails for complete cs train set
dataset = dataset.repeat(DATASET_REPEAT_COUNT) # HACK: to increase the dataset size
dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE) # Always add in the end of the data pipeline
# val ds
val_dataset = tf.data.Dataset.from_tensor_slices(xy_val_paths)
val_dataset = val_dataset.map(lambda x: tf.py_function(func=process_path,
                                                       inp=[x],
                                                       Tout=[tf.uint8, tf.float16]),
                                                       num_parallel_calls=tf.data.experimental.AUTOTUNE)
val_dataset = val_dataset.batch(BATCH_SIZE, drop_remainder=True)

# https://notebook.community/cshallue/models/samples/outreach/blogs/segmentation_blogpost/image_segmentation
def smoothness_loss(y_pred): # total_variation_loss
    # calculate the differences between neighboring pixels
    dy = tf.abs(y_pred[:, 1:, :, :] - y_pred[:, :-1, :, :])
    dx = tf.abs(y_pred[:, :, 1:, :] - y_pred[:, :, :-1, :])
    # calculate the total variation loss
    tv_loss = tf.reduce_mean(dx) + tf.reduce_mean(dy)
    return tv_loss
def dice_coeff(y_true, y_pred):
    smooth = 1.
    # Flatten
    y_true_f = tf.reshape(y_true, [-1])
    y_pred_f = tf.reshape(y_pred, [-1])
    intersection = tf.reduce_sum(y_true_f * y_pred_f)
    score = (2. * intersection + smooth) / (tf.reduce_sum(y_true_f) + tf.reduce_sum(y_pred_f) + smooth)
    return score
def dice_loss(y_true, y_pred):
    loss = 1 - dice_coeff(y_true, y_pred)
    return loss
def MeanIoU(y_true, y_pred):
    mask = tf.cast(tf.not_equal(y_true[:,:,:,MASK_INDEX], 1), tf.float16)
    mask = tf.reshape(mask, (BATCH_SIZE, IMG_HEIGHT, IMG_WIDTH))
    intersection = tf.reduce_sum(tf.multiply(y_true * y_pred, tf.expand_dims(mask, axis=-1)))
    union = tf.reduce_sum(tf.multiply(y_true, tf.expand_dims(mask, axis=-1))) + tf.reduce_sum(tf.multiply(y_pred, tf.expand_dims(mask, axis=-1)))
    MeanIoU= (intersection + 1e-7) / (union + 1e-7)
    return MeanIoU
def dice_loss_new(y_true, y_pred, mask):
    # Calculate the intersection and union
    # intersection = tf.reduce_sum(y_true * y_pred * mask)
    intersection = tf.reduce_sum(tf.multiply(y_true * y_pred, tf.expand_dims(mask, axis=-1)))
    # union = tf.reduce_sum(y_true * mask) + tf.reduce_sum(y_pred * mask)
    union = tf.reduce_sum(tf.multiply(y_true, tf.expand_dims(mask, axis=-1))) + tf.reduce_sum(tf.multiply(y_pred, tf.expand_dims(mask, axis=-1)))
    # Calculate the Dice coefficient
    dice_coef = (2.0 * intersection + 1e-7) / (union + 1e-7)
    # Calculate the Dice loss
    dice_loss = 1.0 - dice_coef
    return dice_loss
def cce_dice_loss(y_true, y_pred):
    # y_true = tf.reshape(y_true, (BATCH_SIZE, IMG_HEIGHT, IMG_WIDTH, NUM_CLASSES))
    # y_pred = tf.reshape(y_pred, (BATCH_SIZE, IMG_HEIGHT, IMG_WIDTH, NUM_CLASSES))
    # loss = smoothness_loss(y_pred)
    # mask = get_mask(y_true)
    # y_true = tf.boolean_mask(y_true, mask)
    # y_pred = tf.boolean_mask(y_pred, mask)
    # loss += tf.keras.losses.categorical_crossentropy(y_true, y_pred) + dice_loss(y_true, y_pred)
    mask = tf.cast(tf.not_equal(y_true[:,:,:,MASK_INDEX], 1), tf.float32)
    mask = tf.reshape(mask, (BATCH_SIZE, IMG_HEIGHT, IMG_WIDTH))
    loss = tf.reduce_mean(tf.keras.losses.categorical_crossentropy(y_true, y_pred) * mask)
    loss += dice_loss_new(y_true, y_pred, mask)
    return loss
# Calculate the regularization loss
def regularization_loss():
    regularization_losses = tf.reduce_sum(model.losses)
    return regularization_losses
def loss_with_regularization(y_true, y_pred):
  return cce_dice_loss(y_true, y_pred) + regularization_loss()

# Learning rate schedule
total_steps = len(xy_train_paths) // BATCH_SIZE * NUM_EPOCHS
lr_scheduler = tf.keras.experimental.CosineDecayRestarts(initial_learning_rate=INITIAL_LR,
                                                         first_decay_steps=total_steps)
# Custom Metric
class MeanIoU(tf.keras.metrics.Metric):
  def __init__(self, name='mean_iou', **kwargs):
    super(MeanIoU, self).__init__(name=name, **kwargs)
    self.num_classes = NUM_CLASSES
    self.total_cm = self.add_weight('total_confusion_matrix', shape=(NUM_CLASSES, NUM_CLASSES), initializer='zeros')
  def update_state(self, y_true, y_pred, sample_weight=None):
    y_true = tf.argmax(y_true, axis=-1)
    y_pred = tf.argmax(y_pred, axis=-1)
    cm = tf.math.confusion_matrix(tf.reshape(y_true, [-1]), tf.reshape(y_pred, [-1]), num_classes=self.num_classes)
    cm = tf.cast(cm, dtype=tf.float32)  # Cast to float dtype
    self.total_cm.assign_add(cm)
  def result(self):
    sum_over_row = tf.reduce_sum(self.total_cm, axis=0)
    sum_over_col = tf.reduce_sum(self.total_cm, axis=1)
    true_positives = tf.linalg.diag_part(self.total_cm)
    denominator = sum_over_row + sum_over_col - true_positives
    epsilon = tf.keras.backend.epsilon()
    iou = true_positives / (denominator + epsilon)  # Add epsilon to avoid division by zero
    mean_iou = tf.reduce_mean(iou)
    return mean_iou
  def reset_state(self):
    tf.keras.backend.set_value(self.total_cm, tf.zeros((self.num_classes, self.num_classes)))

# Compile nn model
def compile_model(model):
  model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=INITIAL_LR),
            loss=loss_with_regularization,
            metrics=[MeanIoU()])
  return model

def load_model(model_path, trainable=False):
  model = tf.keras.models.load_model(model_path,
                                    custom_objects={'loss_with_regularization':loss_with_regularization,
                                                    'MeanIoU': MeanIoU()})

  if trainable:
    model = compile_model(model)
  return model

# Test nn model
def test(model_path, epoch, from_onnx, from_openvino):
  start = time.perf_counter()
  if (not from_onnx):
    model = load_model(model_path)
  for imgs, labels in val_dataset.take(1):
    masked_imgs = apply_mask(imgs, labels, 3)
    cm_labels = get_colormap(labels)
    if (not from_onnx):
      cm_predictions = get_colormap(model.predict(masked_imgs))
    else:
      if from_onnx:
        p = onnx_predict(f'{model_path}/model.onnx', masked_imgs.numpy().astype(np.float32))
      elif from_openvino:
        p = openvino_predict(f'{model_path}/openvino', masked_imgs.numpy().astype(np.float32))
      else:
        p = model.predit(masked_imgs)
      cm_predictions = get_colormap(p)
    masked_cm_predictions = apply_mask(cm_predictions, labels, 3)
    for ind in tf.range(BATCH_SIZE):
      ind = tf.cast(ind, tf.int32)
      img = tf.image.encode_png(tf.squeeze(masked_imgs[ind,:,:,:]))
      tf.io.write_file(f'{model_path}/{ind}_image_masked.png', img)
      label = tf.image.encode_png(cm_labels[ind,:,:,:])
      tf.io.write_file(f'{model_path}/{ind}_groudtruth.png', label)
      prediction = tf.image.encode_png(masked_cm_predictions[ind,:,:,:])
      tf.io.write_file(f'{model_path}/{ind}_{epoch}_prediction_masked.png', prediction)
  print(f'[INFO] Testing Completed in : {time.perf_counter()-start} secs')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--modelpath', help='Abs path of the model dir')
    parser.add_argument('--epoch', nargs='?', help='Epoch Number', default=0)
    parser.add_argument('--from_onnx', nargs='?', help='Is Onnx model used', default=False)
    parser.add_argument('--from_openvino', nargs='?', help='Is OpenVINO IR format model used', default=False)
    args = parser.parse_args()
    modelpath = args.modelpath
    epoch = args.epoch
    from_onnx = args.from_onnx
    from_openvino = args.from_openvino
    test(modelpath, epoch, from_onnx, from_openvino)