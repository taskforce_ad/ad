#! /usr/bin/env python

import os
from os.path import join
from glob import glob
from matplotlib import pyplot

basedir = os.path.abspath(os.path.dirname(__file__))

traindirs = [join('gtFine', 'train'),
             join('gtCoarse', 'train'),
             join('gtCoarse', 'train_extra'),
             join('leftImg8bit', 'train')]

# gtFine/train/aachen
# aachen_000033_000019_gtFine_color.png
# aachen_000033_000019_gtFine_instanceIds.png
# aachen_000033_000019_gtFine_labelIds.png
# aachen_000033_000019_gtFine_labelTrainIds.png
# aachen_000033_000019_gtFine_polygons.json

# gtCoarse/train/aachen
# aachen_000033_000019_gtCoarse_color.png
# aachen_000033_000019_gtCoarse_instanceIds.png
# aachen_000033_000019_gtCoarse_labelIds.png
# aachen_000033_000019_gtCoarse_labelTrainIds.png
# aachen_000033_000019_gtCoarse_polygons.json

# gtCoarse/train_extra/augsburg => NO Color Image Present
# augsburg_000033_000019_gtCoarse_color.png
# augsburg_000033_000019_gtCoarse_instanceIds.png
# augsburg_000033_000019_gtCoarse_labelIds.png
# augsburg_000033_000019_gtCoarse_labelTrainIds.png
# augsburg_000033_000019_gtCoarse_polygons.json

# leftImg8bit/train/aachen
# aachen_000000_000019_leftImg8bit.png

## Remark: we have same cities in Fine and Coarse
## TRAIN
ds_type = 'train'
train_color_imgs = glob(join(basedir, 'leftImg8bit', ds_type, '*', '*.png'))
ds = 'gtFine'
train_fine_labels = [join(basedir, ds, ds_type, os.path.basename(color)[:os.path.basename(color).find('_')],
                  os.path.basename(color)[:os.path.basename(color).rfind('_leftImg8bit.png')]+f"_{ds}_labelTrainIds.png") for color in train_color_imgs]
ds = 'gtCoarse'
train_coarse_labels = [join(basedir, ds, ds_type, os.path.basename(color)[:os.path.basename(color).find('_')],
                  os.path.basename(color)[:os.path.basename(color).rfind('_leftImg8bit.png')]+f"_{ds}_labelTrainIds.png") for color in train_color_imgs]

## TEST
ds_type = 'test'
test_color_imgs = glob(join(basedir, 'leftImg8bit', ds_type, '*', '*.png'))
ds = 'gtFine'
test_fine_labels = [join(basedir, ds, ds_type, os.path.basename(color)[:os.path.basename(color).find('_')],
                  os.path.basename(color)[:os.path.basename(color).rfind('_leftImg8bit.png')]+f"_{ds}_labelTrainIds.png") for color in test_color_imgs]
ds = 'gtCoarse'
test_coarse_labels = [join(basedir, ds, ds_type, os.path.basename(color)[:os.path.basename(color).find('_')],
                  os.path.basename(color)[:os.path.basename(color).rfind('_leftImg8bit.png')]+f"_{ds}_labelTrainIds.png") for color in test_color_imgs]

## VAL
ds_type = 'val'
val_color_imgs = glob(join(basedir, 'leftImg8bit', ds_type, '*', '*.png'))
ds = 'gtFine'
val_fine_labels = [join(basedir, ds, ds_type, os.path.basename(color)[:os.path.basename(color).find('_')],
                  os.path.basename(color)[:os.path.basename(color).rfind('_leftImg8bit.png')]+f"_{ds}_labelTrainIds.png") for color in val_color_imgs]
ds = 'gtCoarse'
val_coarse_labels = [join(basedir, ds, ds_type, os.path.basename(color)[:os.path.basename(color).find('_')],
                  os.path.basename(color)[:os.path.basename(color).rfind('_leftImg8bit.png')]+f"_{ds}_labelTrainIds.png") for color in val_color_imgs]


## TESTING existence


# for color in all_color_imgs:
#     name = os.path.basename(color)
#     name_with_id = name[:name.rfind('_leftImg8bit.png')]
#     city = name[:name.find('_')]

#     fine_label_path = join(basedir, traindirs[0], city, name_with_id + '_gtFine_color.png')
#     coarse_label_path = join(basedir, traindirs[1], city, name_with_id + '_gtCoarse_color.png')

#     pyplot.imshow(fine_label_path[0])
#     pyplot.show()


train_dataset = join(basedir, 'train.dataset')
test_dataset = join(basedir, 'test.dataset')
val_dataset = join(basedir, 'val.dataset')


if os.path.exists(train_dataset):
    print('train.dataset file exist NOTHING created')
else:
    ## WRITE train.dataset file
    train_file = open(train_dataset,"w+")
    for sample in zip(train_color_imgs, train_fine_labels, train_coarse_labels):
        #train_file.write(f"{sample[0]} {sample[1]} {sample[2]}\n")
        if os.path.exists(sample[0]) and os.path.exists(sample[1]):
            train_file.write(f"{sample[0]} {sample[1]}\n")
    train_file.close()

if os.path.exists(test_dataset):
    print('test.dataset file exist NOTHING created')
else:
    ## WRITE train.dataset file
    test_file = open(test_dataset,"w+")
    for sample in zip(test_color_imgs, test_fine_labels, test_coarse_labels):
        #test_file.write(f"{sample[0]} {sample[1]} {sample[2]}\n")
        if os.path.exists(sample[0]) and os.path.exists(sample[1]):
            test_file.write(f"{sample[0]} {sample[1]}\n")
    test_file.close()

if os.path.exists(val_dataset):
    print('val.dataset file exist NOTHING created')
else:
    ## WRITE train.dataset file
    val_file = open(val_dataset,"w+")
    for sample in zip(val_color_imgs, val_fine_labels, val_coarse_labels):
        #val_file.write(f"{sample[0]} {sample[1]} {sample[2]}\n")
        if os.path.exists(sample[0]) and os.path.exists(sample[1]):
            val_file.write(f"{sample[0]} {sample[1]}\n")
    val_file.close()

print("DATASET creation complete")
