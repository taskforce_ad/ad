# AD -- Autonomous Driving for All

### Bounding Box Detection
![](docs/bbox.png)

### Depth Estimation
![](docs/disp.jpg)

# GET STARTED

A mono-repository intended to create a Neural Network specific projects at one place.
We use [VSCode](https://code.visualstudio.com/) as our code editor and debugger.
We use the following package version during all coding purposes
1. tensorflow = 2.0.0
2. python >= 3.6

# INSTALLATION (FRESH LINUX)
## Install distutils
    sudo apt-get install python3-distutils
## Install using [get_pip.py](https://bootstrap.pypa.io/get-pip.py)
    python get-pip.py

# INSTALLATION
## **create a python virtual environment**
### Using virtualenv
    virtualenv -p python3.7 envname
    OR
    python -m pip install virtualenv
    python -m virtualenv envname
### Using venv
    python -m venv envname

## **update your environtment variable**
### For Windows
    add <VIRTUALENV_ROOT> and <VIRTUALENV_ROOT>\Scripts in PATH variable
    add <VIRTUALENV_ROOT>\Lib and <VIRTUALENV_ROOT>\Lib\site-packages in PYTHONPATH variable
### For Linux <-- Not Tested -->
    add <VIRTUALENV_ROOT> and <VIRTUALENV_ROOT>/bin in PATH variable
    add <VIRTUALENV_ROOT>\Lib and <VIRTUALENV_ROOT>\Lib\site-packages in PYTHONPATH variable

## **activate the virtual environment**
### For Windows
    cd <VIRTUALENV_ROOT>\Scripts
    activate
### For Linux
    cd <VIRTUALENV_ROOT>/bin
    source activate

## **install "ad" project requirements**
    git clone https://<BITBUCKET_USERNAME>@bitbucket.org/taskforce_ad/ad.git
    cd ad
    pip install -r requirements.txt

# BUGS and WORKAROUND
## **plot_model(..) not working**
1. change "os.errno" to "errno" in <VENV_ROOT>\Lib\site-packages\pydot.py
   1. add "import errno"
   2. change "os.errno" to "errno" in <VENV_ROOT>\Lib\site-packages\pydot.py
2. install [graphviz](http://www.graphviz.org/download/) from here
3. set the <GRAPHVIZ_ROOT>\bin to the PATH variable
4. re-start the Python IDE

## **Setup Cityscapes dataset helpers and evaluation tools**
1. Download and extract the scripts from https://github.com/mcordts/cityscapesScripts
2. Copy the extracted folder inside cityscapes_helper_root = ad\dev\data\cityscapesScripts
3. cd <cityscapes_helper_root>
4. pip install cityscapesscripts

  This will install cityscapesscripts-2.1.7 pyquaternion-0.9.9 typing-3.7.4.3

# Docker Support
## Run docker container, navigate to .devcontainer and call the scripts in the order:
1. build_dev.bat
2. build.bat
3. run.bat

## Install pip dependencies (Inside docker container)
We use pipenv for python virtual environment management

1. cd ~/ad
2. pipenv install
3. pipenv shell

## Train a semseg network using cityscapes test_data (Inside docker container, Inside (ad) virtual environment)
### set the path inside ad/dev/semseg/parameters.py

   1. 'dataset_basedir': '/workspace/ad/data/test_data/semseg/cityscapes'
### Inside docker

   1. cd ~/ad/dev/semseg
   3. python train.py