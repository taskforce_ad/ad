import os
# print(os.environ['PYTHONPATH'])
# os.environ['PYTHONPATH'] += '/home/dawudmaxx/ad'
from os.path import *
import shutil
from PIL import Image
import numpy as np
import threading
import time
import calendar
import math
import pickle
import subprocess
from functools import partial
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
import socket

import tensorflow as tf
from tensorflow.keras.layers import *
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.applications import ResNet50
from tensorflow.keras.callbacks import LearningRateScheduler

from utils.init_module import INIT
from dev.data.cityscapes_dataset import get_colormap
from utils.pickel import decode_and_plot_history

ad_root = INIT(memory_growth=False).get_ad_root()
timestamp = calendar.timegm(time.gmtime())

# Load dataset
INVALID_LABEL = 255
NUM_CLASSES = 20
VOID_LABEL = NUM_CLASSES - 1
MASK_INDEX = VOID_LABEL
IMG_HEIGHT = 512
IMG_WIDTH = 1024
IMG_CHANNELS = 3
BATCH_SIZE = 2
INITIAL_LR = 0.01
MAX_LR = 0.0001
STEP_SIZE_FOR_ONE_LR_CYCLE = 5
DATASET_REPEAT_COUNT = 1
L2_REGULARIZER = tf.keras.regularizers.l2(0.1)
NUM_EPOCHS = 100
LOGGING_FREQUENCY = 5
MODEL_NAME=f'model-semseg-{timestamp}-epoch{NUM_EPOCHS}'
HISTORY_FILENAME = 'history.pickle'
# DATASETFILE_DIR_PATH = f'{ad_root}/data/test_data/semseg/cityscapes'
DATASETFILE_DIR_PATH = '/mnt/y/DATA/cityscapes'
RESUME_MODEL_NAME = None # 'model-semseg-1686334070-epoch150'
OVERWRITE_MODEL = True

def split_X_Y(dataset_filename, datasetdir):
  XY = []
  with open(dataset_filename) as ds_file:
    lines = ds_file.readlines()
    XY = [(join(datasetdir, line.split(" ")[0].rstrip()),
           join(datasetdir, line.split(" ")[1].rstrip())) for line in lines]
  return XY

def read_dataset(datasetdir):
  XY_train_list = split_X_Y(join(datasetdir, 'train.dataset'), datasetdir)
  XY_test_list = split_X_Y(join(datasetdir, 'test.dataset'), datasetdir)
  XY_val_list = split_X_Y(join(datasetdir, 'val.dataset'), datasetdir)
  return XY_train_list, XY_test_list, XY_val_list

def load_img(filepath):
  img = tf.image.decode_png(tf.io.read_file(filepath), channels=3)
  img = tf.image.resize(img, [IMG_HEIGHT, IMG_WIDTH])
  img = tf.cast(img, tf.uint8)
  return img

def load_and_encode_label(filepath):
  label = tf.image.decode_png(tf.io.read_file(filepath), channels=1)
  label = tf.where(label == INVALID_LABEL, tf.constant(VOID_LABEL, dtype=label.dtype), label)
  label = to_categorical(label, num_classes=NUM_CLASSES)
  label = tf.cast(tf.image.resize(label, [IMG_HEIGHT, IMG_WIDTH]), tf.bool)
  label = tf.cast(label, tf.float16)
  return label

def get_mask(y_true):
  mask = tf.logical_not(tf.cast(y_true[:,:,:,MASK_INDEX], tf.bool))
  return mask

def apply_mask(inputs, y_true, inputs_channels):
  mask = get_mask(y_true)
  mask = tf.tile(tf.expand_dims(mask, -1), [1,1,1,inputs_channels])
  inputs = tf.where(mask, inputs, tf.constant(0, dtype=inputs.dtype))
  return inputs

xy_train_paths, xy_test_paths, xy_val_paths = read_dataset(DATASETFILE_DIR_PATH)
# make val set smaller
xy_val_paths = xy_val_paths[0:30]
print(f'[INFO] Size of train set: {len(xy_train_paths)}')
print(f'[INFO] Size of val set: {len(xy_val_paths)}')

# Create dataset
def process_path(sample):
  img_path, label_path = sample[0], sample[1]
  img = load_img(img_path)
  label = load_and_encode_label(label_path)
  return img, label
dataset = tf.data.Dataset.from_tensor_slices(xy_train_paths)
dataset = dataset.map(lambda x: tf.py_function(func=process_path,
                                               inp=[x],
                                               Tout=[tf.uint8, tf.float16]),
                                               num_parallel_calls=tf.data.experimental.AUTOTUNE)
dataset = dataset.batch(BATCH_SIZE, drop_remainder=True)
# dataset = dataset.shuffle(len(xy_train_paths), reshuffle_each_iteration=True) # !! fails for complete cs train set
dataset = dataset.repeat(DATASET_REPEAT_COUNT) # HACK: to increase the dataset size
dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE) # Always add in the end of the data pipeline
# val ds
val_dataset = tf.data.Dataset.from_tensor_slices(xy_val_paths)
val_dataset = val_dataset.map(lambda x: tf.py_function(func=process_path,
                                                       inp=[x],
                                                       Tout=[tf.uint8, tf.float16]),
                                                       num_parallel_calls=tf.data.experimental.AUTOTUNE)
val_dataset = val_dataset.batch(BATCH_SIZE, drop_remainder=True)

# Create nn model
def decoder_block(num_of_kernels):
  return Sequential([UpSampling2D((2,2)),
                    Conv2D(num_of_kernels, 2, activation='relu', padding='same'),
                    BatchNormalization(),
                    Conv2D(num_of_kernels/2, 2, activation='relu', padding='same'),
                    BatchNormalization(),
                    Dropout(.1),
                    ])
def create_model(num_classes, input_shape):
    # Define the PyDNet encoder layers
    encoder = ResNet50(weights='imagenet', include_top=False, input_shape=input_shape)

    # Set the encoder layers to be non-trainable
    for layer in encoder.layers:
        layer.trainable = False

    # Get the output tensor from one of the intermediate layers
    encoder_output = encoder.get_layer('conv4_block6_out').output

    # Upsampling block
    x = Conv2D(512, 1, padding='same', activation='relu')(encoder_output)
    x = UpSampling2D(2)(x)

    # Add additional convolutional blocks for segmentation
    x = Conv2D(256, 3, padding='same', activation='relu')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    x = Conv2D(128, 3, padding='same', activation='relu')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    x = Conv2D(64, 3, padding='same', activation='relu')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    x = decoder_block(64)(x)
    x = decoder_block(64)(x)
    x = decoder_block(32)(x)
    # x = decoder_block(32)(x)

    # Final prediction layer
    outputs = Conv2D(num_classes, 1, activation='softmax')(x)

    # Create the model
    model = Model(inputs=encoder.input, outputs=outputs)

    return model

# Assuming you have `x_train` and `y_train` as your training data and labels
# and `x_val` and `y_val` as your validation data and labels

input_shape = (IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS)
num_classes = NUM_CLASSES
# Create the model
model = create_model(num_classes=num_classes, input_shape=input_shape)
# add L2 regularization to your model layers
for layer in model.layers:
    if hasattr(layer, 'kernel_regularizer'):
        layer.kernel_regularizer = L2_REGULARIZER
model.summary()



# https://notebook.community/cshallue/models/samples/outreach/blogs/segmentation_blogpost/image_segmentation
def smoothness_loss(y_pred): # total_variation_loss
    # calculate the differences between neighboring pixels
    dy = tf.abs(y_pred[:, 1:, :, :] - y_pred[:, :-1, :, :])
    dx = tf.abs(y_pred[:, :, 1:, :] - y_pred[:, :, :-1, :])
    # calculate the total variation loss
    tv_loss = tf.reduce_mean(dx) + tf.reduce_mean(dy)
    return tv_loss
def dice_coeff(y_true, y_pred):
    smooth = 1.
    # Flatten
    y_true_f = tf.reshape(y_true, [-1])
    y_pred_f = tf.reshape(y_pred, [-1])
    intersection = tf.reduce_sum(y_true_f * y_pred_f)
    score = (2. * intersection + smooth) / (tf.reduce_sum(y_true_f) + tf.reduce_sum(y_pred_f) + smooth)
    return score
def dice_loss(y_true, y_pred):
    loss = 1 - dice_coeff(y_true, y_pred)
    return loss
def MeanIoU(y_true, y_pred):
    mask = tf.cast(tf.not_equal(y_true[:,:,:,MASK_INDEX], 1), tf.float16)
    mask = tf.reshape(mask, (BATCH_SIZE, IMG_HEIGHT, IMG_WIDTH))
    intersection = tf.reduce_sum(tf.multiply(y_true * y_pred, tf.expand_dims(mask, axis=-1)))
    union = tf.reduce_sum(tf.multiply(y_true, tf.expand_dims(mask, axis=-1))) + tf.reduce_sum(tf.multiply(y_pred, tf.expand_dims(mask, axis=-1)))
    MeanIoU= (intersection + 1e-7) / (union + 1e-7)
    return MeanIoU
def dice_loss_new(y_true, y_pred, mask):
    # Calculate the intersection and union
    # intersection = tf.reduce_sum(y_true * y_pred * mask)
    intersection = tf.reduce_sum(tf.multiply(y_true * y_pred, tf.expand_dims(mask, axis=-1)))
    # union = tf.reduce_sum(y_true * mask) + tf.reduce_sum(y_pred * mask)
    union = tf.reduce_sum(tf.multiply(y_true, tf.expand_dims(mask, axis=-1))) + tf.reduce_sum(tf.multiply(y_pred, tf.expand_dims(mask, axis=-1)))
    # Calculate the Dice coefficient
    dice_coef = (2.0 * intersection + 1e-7) / (union + 1e-7)
    # Calculate the Dice loss
    dice_loss = 1.0 - dice_coef
    return dice_loss
def cce_dice_loss(y_true, y_pred):
    # y_true = tf.reshape(y_true, (BATCH_SIZE, IMG_HEIGHT, IMG_WIDTH, NUM_CLASSES))
    # y_pred = tf.reshape(y_pred, (BATCH_SIZE, IMG_HEIGHT, IMG_WIDTH, NUM_CLASSES))
    # loss = smoothness_loss(y_pred)
    # mask = get_mask(y_true)
    # y_true = tf.boolean_mask(y_true, mask)
    # y_pred = tf.boolean_mask(y_pred, mask)
    # loss += tf.keras.losses.categorical_crossentropy(y_true, y_pred) + dice_loss(y_true, y_pred)
    mask = tf.cast(tf.not_equal(y_true[:,:,:,MASK_INDEX], 1), tf.float32)
    mask = tf.reshape(mask, (BATCH_SIZE, IMG_HEIGHT, IMG_WIDTH))
    loss = tf.reduce_mean(tf.keras.losses.categorical_crossentropy(y_true, y_pred) * mask)
    loss += dice_loss_new(y_true, y_pred, mask)
    return loss
# Calculate the regularization loss
def regularization_loss():
    regularization_losses = tf.reduce_sum(model.losses)
    return regularization_losses
def loss_with_regularization(y_true, y_pred):
  return cce_dice_loss(y_true, y_pred) + regularization_loss()

# Learning rate schedule
def cyclic_learning_rate(epoch, initial_lr, max_lr, step_size):
    cycle = math.floor(1 + epoch / (2 * step_size))
    x = abs(epoch / step_size - 2 * cycle + 1)
    lr = initial_lr + (max_lr - initial_lr) * max(0, (1 - x))
    return lr
lr_scheduler = LearningRateScheduler(lambda epoch: cyclic_learning_rate(epoch, INITIAL_LR, MAX_LR, STEP_SIZE_FOR_ONE_LR_CYCLE))

# Custom Metric
class MeanIoU(tf.keras.metrics.Metric):
  def __init__(self, name='mean_iou', **kwargs):
    super(MeanIoU, self).__init__(name=name, **kwargs)
    self.num_classes = NUM_CLASSES
    self.total_cm = self.add_weight('total_confusion_matrix', shape=(NUM_CLASSES, NUM_CLASSES), initializer='zeros')
  def update_state(self, y_true, y_pred, sample_weight=None):
    y_true = tf.argmax(y_true, axis=-1)
    y_pred = tf.argmax(y_pred, axis=-1)
    cm = tf.math.confusion_matrix(tf.reshape(y_true, [-1]), tf.reshape(y_pred, [-1]), num_classes=self.num_classes)
    cm = tf.cast(cm, dtype=tf.float32)  # Cast to float dtype
    self.total_cm.assign_add(cm)
  def result(self):
    sum_over_row = tf.reduce_sum(self.total_cm, axis=0)
    sum_over_col = tf.reduce_sum(self.total_cm, axis=1)
    true_positives = tf.linalg.diag_part(self.total_cm)
    denominator = sum_over_row + sum_over_col - true_positives
    epsilon = tf.keras.backend.epsilon()
    iou = true_positives / (denominator + epsilon)  # Add epsilon to avoid division by zero
    mean_iou = tf.reduce_mean(iou)
    return mean_iou
  def reset_state(self):
    tf.keras.backend.set_value(self.total_cm, tf.zeros((self.num_classes, self.num_classes)))

# Compile nn model
# optimizer = tf.keras.optimizers.Adam(learning_rate=INITIAL_LR)
def compile_model(model):
  model.compile(optimizer='adam',
            loss=loss_with_regularization,
            metrics=[MeanIoU()])
  return model

def load_model(model_name, trainable=False):
  model = tf.keras.models.load_model(f'{ad_root}/{model_name}',
                                    custom_objects={'loss_with_regularization':loss_with_regularization,
                                      'MeanIoU': MeanIoU()})

  if trainable:
    model = compile_model(model)
  return model

# Test nn model
def test(model_name, epoch=""):
  model = load_model(model_name)
  for imgs, labels in val_dataset.take(1):
    masked_imgs = apply_mask(imgs, labels, 3)
    cm_labels = get_colormap(labels)
    cm_predictions = get_colormap(model.predict(masked_imgs))
    masked_cm_predictions = apply_mask(cm_predictions, labels, 3)
    for ind in tf.range(BATCH_SIZE):
      ind = tf.cast(ind, tf.int32)
      img = tf.image.encode_png(tf.squeeze(masked_imgs[ind,:,:,:]))
      tf.io.write_file(f'{ad_root}/{model_name}/{ind}_image_masked.png', img)
      label = tf.image.encode_png(cm_labels[ind,:,:,:])
      tf.io.write_file(f'{ad_root}/{model_name}/{ind}_groudtruth.png', label)
      prediction = tf.image.encode_png(masked_cm_predictions[ind,:,:,:])
      tf.io.write_file(f'{ad_root}/{model_name}/{ind}_{epoch}_prediction_masked.png', prediction)
  print(f'[INFO] Every Epoch Testing Done')

# read the last slack message and send to slack
client = WebClient(token='xoxb-5378520539671-5400326517156-JxtajjXOwSsHxgpTXRX6CTNz', timeout=15)
channel_id = 'C05BJTK442X'  # Replace with the channel ID or name
def send_to_slack(model_name):
  try:
    response = client.conversations_history(channel=channel_id, limit=1)
    messages = response['messages']
    for message in messages:
      if message['text'].lower() == 'now':
        if not exists(f'{ad_root}/{model_name}/{HISTORY_FILENAME}'):
          response = client.chat_postMessage(channel=channel_id, text=f'[WAIT] No {HISTORY_FILENAME} present, Try again later')
          if not response['ok']:
            print('[ERROR] Failed to send message:', response['error'])
          break
        decode_and_plot_history(f'{ad_root}/{model_name}', False)
        try:
          response = client.files_upload(channels=channel_id,
                                         file=f'{ad_root}/{model_name}/history_plot.png',
                                         initial_comment='Loss And MeanIoU!')
          if not response['ok']:
            print('[ERROR] Failed to send image.')
        except SlackApiError as e:
          print(f"[ERROR] Error sending image: {e.response['error']}")
  except SlackApiError as e:
    print(f"[ERROR] Error retrieving message history: {e.response['error']}")
  except socket.timeout:
    print("[ERROR] Socket timeout occurred")
    response = client.chat_postMessage(channel=channel_id, text=f'[ERROR] Socket timeout occurred')
    if not response['ok']:
      print('[ERROR] Failed to send message:', response['error'])
# run slack in a separate thread with 10 sec wait
def slack_thread_function():
  model_name = MODEL_NAME
  if RESUME_MODEL_NAME != None and OVERWRITE_MODEL:
    model_name = RESUME_MODEL_NAME
  while True:
    # print('[INFO] Slack looking NOW')
    # print(f'[DEBUG] In slack thread model_name: {model_name}')
    send_to_slack(model_name)
    # sleep for 10 seconds
    time.sleep(10)

# Update histroy and dump into history.pickle
def update_histroy(model_name, logs, epoch):
  if exists(f'{ad_root}/{model_name}/{HISTORY_FILENAME}'):
    with open(f'{ad_root}/{model_name}/{HISTORY_FILENAME}', 'rb') as history_file:
      history = pickle.load(history_file)
  else:
    history = {}
  if not str(timestamp) in history:
      history[str(timestamp)] = []
  history[str(timestamp)].append(logs)
  with open(f'{ad_root}/{model_name}/{HISTORY_FILENAME}', 'wb') as history_file:
    pickle.dump(history, history_file)
  # send to slack
  # send_to_slack(model_name) # run in separate thread

# Train nn model
def train(model, resume_model_name=None, overwrite=False):
  model_name = MODEL_NAME
  if resume_model_name != None:
    print(f'[DEBUG] Resuming Model {resume_model_name} with Overwrite: {overwrite}')
    model = load_model(resume_model_name, trainable=True)
    if overwrite:
      model_name = resume_model_name
  else:
    compile_model(model)

  # start slack thread
  my_thread = threading.Thread(target=slack_thread_function)
  my_thread.start()

  # call test on the saved model after every epoch
  class CustomCallback(tf.keras.callbacks.Callback):
    first_call = False
    def __init__(self):
      self.first_call = True
    def on_epoch_end(self, epoch, logs=None):
        start = time.perf_counter()
        if (self.first_call):
          # preserve the nn code for reproducing results
          src_filename = basename(__file__)
          source_file = abspath(__file__)
          dot_index = src_filename.rfind('.')
          dest_filename = src_filename[:dot_index] + "-" + str(timestamp) + src_filename[dot_index:]
          destination_file = f'{ad_root}/{model_name}/{dest_filename}'
          shutil.copy(source_file, destination_file)
          # copy over history.pickle from resumued model_name
          if (resume_model_name != None and not overwrite):
            source_file = f'{ad_root}/{resume_model_name}/{HISTORY_FILENAME}'
            destination_file = f'{ad_root}/{model_name}/{HISTORY_FILENAME}'
            if exists(source_file):
              if shutil.copy(source_file, destination_file) != None:
                print(f'[INFO] {basename(source_file)} Resumed')
        # test with 1 val batch
        if epoch % LOGGING_FREQUENCY == 0:
          test(model_name, epoch=epoch) # can be done in parallel
        # update history
        update_histroy(model_name, logs, epoch)
        # preserve the current model and meta infos
        # subprocess.run(['git', 'add', f'{ad_root}/{model_name}'])
        self.first_call = False
        print(f'[DEBUG] OnEpochEnd Execution Time: {time.perf_counter() - start}')

  checkpoint = tf.keras.callbacks.ModelCheckpoint(filepath=f'{ad_root}/{model_name}',
                                                  monitor='mean_iou',
                                                  verbose=True,
                                                  save_best_only=True,
                                                  mode='max')
  callbacks = [checkpoint, CustomCallback(), lr_scheduler]

  model.fit(dataset,
            batch_size=BATCH_SIZE,
            epochs=NUM_EPOCHS,
            validation_data=val_dataset,
            callbacks=callbacks)

  # save plot
  decode_and_plot_history(f'{ad_root}/{model_name}', show_plot=False)

train(model, RESUME_MODEL_NAME, OVERWRITE_MODEL)


